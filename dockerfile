FROM node:lts-alpine as builder

WORKDIR /usr/src/app
COPY ./tsconfig.json ./
COPY ./package.json* ./
COPY ./src ./src/
RUN npm install
RUN node_modules/.bin/tsc


FROM node:lts-alpine
ENV NODE_ENV=production
WORKDIR /usr/build/app
COPY ./package.json* ./
RUN npm install --production
COPY --from=builder /usr/src/app/build/ ./build/

CMD [ "node", "build/index.js" ]
